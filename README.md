# Sandbox environment

## create docker image
```bash
docker build -t registry.gitlab.com/jurafejfar/sandbox .
docker login registry.gitlab.com/jurafejfar/sandbox
docker push registry.gitlab.com/jurafejfar/sandbox
```

## run, work with & destroy image
```bash
docker image ls
docker run -i -t -v `pwd`:/home/myuser/host_work_dir -p 5432:5432 --name sandbox --hostname sandbox registry.gitlab.com/jurafejfar/sandbox /bin/bash

docker container ls -a
docker start pgcontainer
docker attach pgcontainer
docker container rm -v pgcontainer
```

for GUI apps with VcXsrv X Server
```
docker run -i -t -v `pwd`:/home/myuser/host_work_dir -p 5432:5432 --name sandbox --hostname sandbox -e DISPLAY=192.168.77.41:0.0 registry.gitlab.com/jurafejfar/sandbox /bin/bash
```

## clean-up
```bash
docker rm `docker ps -aq`
docker system prune -a
```

## postgres set-up
```
sudo service postgresql start
sudo su - postgres
psql -c "CREATE USER myuser;"
psql -c "CREATE DATABASE myuser;"
```