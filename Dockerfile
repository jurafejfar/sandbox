FROM debian:buster
MAINTAINER Jiří Fejfar <jurafejfar@gmail.com> 

RUN apt update && apt dist-upgrade -y

RUN apt install -y locales
RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN useradd --create-home --shell /bin/bash myuser
RUN echo myuser:mypwd | chpasswd

RUN apt install -y sudo
RUN usermod -a -G sudo myuser

RUN echo "myuser ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user && echo "myuser ALL=(postgres) NOPASSWD:ALL" >> /etc/sudoers.d/user && chmod 0440 /etc/sudoers.d/user

# Set the working directory to app home directory
WORKDIR /home/myuser

# Expose port for possible future PostgreSQL installation
# RUN apt install -y postgresql-11
EXPOSE 5432

# Specify the user to execute all commands below
USER myuser
